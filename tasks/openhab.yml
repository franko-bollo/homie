
- name: "ensure openhab group"
  become: yes
  group:
    name: openhab
    gid: 9001
    state: present
  tags:
    - openhab

- name: add openhab user
  become: yes
  user:
    name: openhab
    shell: /sbin/nologin
    uid: 9001
    state: present
    group: openhab
  tags:
    - openhab

- name: Prepare OpenHab directories
  become: yes
  file:
    path: "{{ item }}"
    mode: 0777
    state: directory
    owner: openhab
    group: openhab
  with_items:
  - "/home/openhab/addons"
  - "/home/openhab/conf"
  - "/home/openhab/conf/scripts"
  - "/home/openhab/conf/services"
  - "/home/openhab/conf/sitemaps"
  - "/home/openhab/workspace"
  - "/home/openhab/logs"
  - "/home/openhab/persistence"
  - "/home/openhab/transform"
  tags:
    - openhab


#- name: copy openhab config
#  copy:
#    src: ../files/openhab/conf/
#    dest: /home/openhab/conf
#    owner: openhab
#    group: openhab
#  become: yes
#  tags:
#    - openhab
#    - copy-config

- name: create openhab services
  become: yes
  template: src=../templates/home/openhab/conf/services/{{item}}.cfg dest=/home/openhab/conf/services/{{item}}.cfg
  with_items:
    - addons
    - influxdb
    - mqtt
    - runtime
  tags:
    - openhab
    - services

- name: configure openhab bindings
  become: yes
  template: src=../templates/home/openhab/userdata/config/binding/{{item}}.config dest=/home/openhab/userdata/config/binding/{{item}}.config
  with_items:
    - miio
  tags:
    - bindingconfig


- name: create openhab things
  become: yes
  template: src=../templates/home/openhab/conf/things/{{item}}.things dest=/home/openhab/conf/things/{{item}}.things
  with_items:
    - velux
    - shelly
    - amazonechocontrol
    - astro
    - harmony
    - homematic
    - hue
    - mail
    - miele
    - miio
    - mqtt
    - netatmo
    - openweathermap
    - telegram

  tags:
    - openhab
    - things

- name: create openhab items
  become: yes
  template: src=../templates/home/openhab/conf/items/{{item}}.items dest=/home/openhab/conf/items/{{item}}.items
  with_items:
    - amazonechocontrol
    - astro
    - harmony
    - homematic
    - hue
    - hueemulation
    - miele
    - miio
    - miflora
    - mqtt
    - netatmo
    - openweathermap
    - speedtest
    - system
    - velux
  tags:
    - openhab
    - items

- name: create openhab maps
  become: yes
  template:
    src: "{{ item }}"
    dest: "/home/openhab/conf/transform/{{ item | basename }}"
  with_fileglob:
    - ../templates/home/openhab/conf/transform/*.map
  tags:
    - openhab
    - maps
    - transform

- name: create openhab persistence
  become: yes
  template: src=../templates/home/openhab/conf/persistence/{{item}}.persist dest=/home/openhab/conf/persistence/{{item}}.persist
  with_items:
    - influxdb
  tags:
    - openhab
    - persistence

- name: create openhab rules
  become: yes
  template: src=../templates/home/openhab/conf/rules/{{item}}.rules dest=/home/openhab/conf/rules/{{item}}.rules
  with_items:
    - amazonechocontrol
    - astro
    - check_battery_cronjob
    - cronjob
    - frontdoor
    - heating
    - hueemulation
    - lights
    - mqtt
    - presence
    - roborock
    - rollershutter
    - speedtest
  tags:
    - openhab
    - rules

- name: create homematic heating groups
  become: yes
  template: src=../templates/home/openhab/conf/items/heating_group.items dest=/home/openhab/conf/items/heating_group.items
  tags:
    - heating_group_items

- name: create openhab sitemaps
  become: yes
  template: src=../templates/home/openhab/conf/sitemaps/{{item}}.sitemap dest=/home/openhab/conf/sitemaps/{{item}}.sitemap
  with_items:
    - heating
    - homematic
    - hue
    - miio
    - netatmo
    - speedtest
    - strom
    - openweathermap
  tags:
    - openhab
    - sitemaps

#- name: download addons
#  get_url:
#    url: "https://github.com/BjoernLange/openhab-miele-cloud-binding-beta/releases/download/v1.0.0-rc.1/org.openhab.binding.mielecloud-1.0.0-rc.1.jar"
#    dest: "/home/openhab/addons"
#  tags:
#    - openhab
#    - addons


- name: copy openhab docker file
  become: yes
  template:
    src: ../templates/home/openhab/{{item}}
    dest: /home/openhab/{{item}}
  with_items:
    - Dockerfile
  tags:
    - openhab
    - openhab-docker

- name: build openhab container image
  become: yes
  docker_image:
    name: myopenhab:v1.0.4
    build:
      path: /home/openhab
    state: present
  tags:
    - openhab
    - openhab-docker



- name: Create and run container for OpenHab
  become: yes
  docker_container:
#    image: openhab/openhab:3.3.0.M1
    image: myopenhab:v1.0.4
#    image: openhab/openhab:3.0.2
#    image: openhab/openhab:2.5.12
    name: openhab3
    state: started
    restart: true
    env:
      "EXTRA_JAVA_OPTS": "-Xms256m -Xmx512m"
    restart_policy: always
    ports:
#    - "80:8080"
    - "8101:8101"
    - "1900:1900/udp"
    - "5000:5000/udp"
    - "8080:8080"
    - "443:8443"
    - "5555:5555"
    volumes:
    - "/etc/localtime:/etc/localtime:ro"
    - "/etc/timezone:/etc/timezone:ro"
    - "/home/openhab/conf:/openhab/conf"
    - "/home/openhab/userdata:/openhab/userdata"
    - "/home/openhab/addons:/openhab/addons"
#    network_mode: host
#    purge_networks: yes
    networks:
#      - name: bridge
      - name: mynetwork
        aliases:
          - openhab
  tags:
    - openhab
    - openhab-docker
