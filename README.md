# Step 1 Rasbian installieren

Siehe hier `https://www.raspberrypi.org/downloads/raspbian/`

Nach Start Sprache/Netzwerk konfigurieren und
```
sudo systemctl enable ssh
sudo systemctl start ssh
```

# Playbook starten

zuerst ssh-fingerprint in ~/.ssh/known_host eintragen (z.b. durch einmaliges einloggen)
ssh pi@1.2.3.4

 ansible-playbook -i ansible/inventory playbooks/mypi.yaml --tags "items"


Hacky workaround, wenn sich das Docker-Netzwerk nicht installieren lässt

Fehler:
```
... The error was: No module named ssl_match_hostname"}
```

Workaround:
```
 cp -a /usr/local/lib/python2.7/dist-packages/backports/ssl_match_hostname /usr/lib/python2.7/dist-packages/backports/
```

# Cloud
Userdaten aus dem openhab Docker hier eintragen:
https://myopenhab.org/account

cat /openhab/userdata/openhabcloud/secret
cat /openhab/userdata/uuid

 docker run -d --rm -v /home/homegear/etc:/etc/homegear:Z -v /home/homegear/lib:/var/lib/homegear:Z -v /home/homegear/log:/var/log/homegear:Z -e TZ=Europe/Berlin -e HOST_USER_ID=$(id -u) -e HOST_USER_GID=$(id -g) -p 2001:2001 -p 2002:2002 -p 2003:2003 --name homegear homegear/rpi-homegear:nightly

## Homematic

Erst Wandthermostat mit Thermostaten und dann mit homegear pairen
Stichworte:
 - docker exec -ti homegear homegear -r (fs 0, families select 0)
 - Um ein Thermostat mit einem Fenster Kontakt zu pairen und peeren
   1) alles reseten (in homegear peers reset 123 und warten, bis Gerät nicht mehr gelistet wird). Fensterkontakte sind zurückgesetzt, wenn ein Kontakt mit einem Magneten mit einem einmaligen orangen Blinken quitiert wird.
      Fensterkontakt reseten: Knopf 5 Sek drücken, es blinkt langsam rot, loslassen, Knopf nochmal 5 Sek
      drücken, schnelles rotes blinken.
      Reset Wand oder Heizunungsthermostat ging erst nach Batterie Entnahme, alle Knöpfe drücken, Batterien wieder rein, bis RES erscheint
   2) Wandthermostat peering starten, Heizungsthermostat Peering starten
   3) Heizungsthermostat Peering starten (Boost Taste für min 3 Sekunden), Kontakt Peering starten
   3) Wandthermostat Peering starten, Kontakt Peering starten
   4) Test des Fenster auf/zu
   5) Homegear `pairing on`, Wandthermostat pairing starten
   6) Homegear `pairing on`, Heizungsthermostat pairing starten
   7) Homegear `pairing on`, Kontakt pairing starten


 - `homegear -r`, dann `families select 0` und `pairing on`
 - Boost-Taste 3 Sek drücken
 - peers setname 2 "Heizungsthermostat Wohnzimmer"

### Homematic Offset
https://forum.homegear.eu/t/homematic-homegear-temperatur-offset-einstellen/3691/2


## MQTT

Test:  mosquitto_sub -d -h 192.168.178.52 -t "/ESP_Stromzaehler/Stromzaehler/Total"

## links
https://raspberry.tips/openhab/openhab-als-alternative-zu-fhem-auf-meinem-raspberry-pi/
https://www.worldoftech.de/article/24-openhab-8-steuerung-mit-amazon-alexa/
https://onesmarthome.de/smart-home-openhab-2-harmony-sprachsteuerung/
https://www.laub-home.de/wiki/Raspberry_Pi_mit_openHAB_als_Smart_Home_Schaltzentrale

Gutes openhab Config Beispiel
https://github.com/ThomDietrich/openhab-config

Script für das Erstellen des Wochenplans
git clone https://github.com/braveheuel/hm-prowee.git

Script Editor zum Erstellen von Wochenprofilen
https://forum.homegear.eu/t/homegear-script-editor/593/18

Channel Referenz etc
https://ref.homegear.eu/family.html?familyLink=homematicbidcos&familyName=HomeMatic+BidCoS

Beispiel rules
https://github.com/openhab/openhab1-addons/wiki/Samples-Rules

Liste aller Homematic Geräte, Kanäle, etc
https://homematic.simdorn.net/wp-content/uploads/2016/03/hm_devices_Endkunden.pdf

Strom

https://www.letscontrolit.com/wiki/index.php/ESPEasy
Download ESPEasy here https://github.com/letscontrolit/ESPEasy/releases
Flashtool für esp32 https://www.espressif.com/en/support/download/other-tools
Die Datei
ESP_Easy_mega_20201022_test_ESP32_4M316k-factory.bin (Im Test ist der Pulse-Driver drin)
an Adresse 0x0000 flashen
Wifi mit dem AP esp-easy verbinden (PW: configesp)
Wifi konfigurieren
Wieder mit normalem WLAN verbinden
Hostnamen konfigurieren ESP_Stromzaehler
openHAB MQTT Controller hinzufügen
Controller Publish /%sysname%/%tskname%/%valname%

Generic-Pulse-Device anlegen,
GPIO z.B. 14
Debounce Time (mSec): 60
Counter Type: Delta/Total/Time
Mode Type: Falling
Intervall: 1sec
Controller: 1

Enablen bei Device und Controller nicht vergessen

https://zukunftathome.de/stromzaehler-und-aktuellen-stromverbrauch-in-openhab-darstellen/

Script, um Ferien-iCal zu migrieren
https://github.com/Rosi2143/EphemerisCanlendarImport

# KLF200 / Velux / Somy Troube Shooting
Symptom:
- mit der My-Taste lässt sich keine Position speichern
- KLF200 erkennt zwar die Rollläden, kann sie auch identifizieren, kann sie aber nicht steuern und auch kein Programm aufnehmen (oder nur von einem Rolladen das Programm aufnehmen)
- KLR200 kann zwar komplett hoch/runter aber nicht iwo in die Mitte steuern

Vor irgendwelchen Resets:
- Kann KLF200 die Rollläden identifizieren?
- My-Programmierung checken

Wahrscheinlich sind die Endlagen des Rollladen verloren gegangen.
Paar mal hoch/runter fahren und My-Programmierung testen. Wenn es nicht hilft:
- Reset des Motors, vielleicht auch beide.
- Während der Initialisierungsphase zuerst mit der KLF200 anlernen (WLAN auf die KLF, im Browser http://klf200.velux/ aufrufen)
- Falls WLAN nicht an, KLF booten (ist normalerweise nur 10min im WLAN Modus)
- Aus der klf heraus die Rollläden suchen (Neues Produkt). U.U vorher altes löschen.
- Bei den resetteten Rollladen eine "Bedienung registrieren" (klf Oberfläche), danach auf der Smoove FB kurz die Prog Taste drücken
- Der Init-Anleitung der Oximo-Motoren folgen (voll-Automatische Endlagen Einstellung)
- 2 / 3 Mal die Rollläden hoch/runter fahren
- Versuchen, die My-Taste in der Mitte zu programmieren.
Anmeldung von Somfy-IO Motoren funktionierte nach dem 3./4. Versuch
Als SLIP-Passwort gerne mal das wlan der KLF200 Passwort testen

Tool zur USB Kontrolle:
https://github.com/mvp/uhubctl

# MiFlora
Die MiFloras werden mit einem esp32+OpenMQTTGateway ausgelesen (bessere Empfang als mit dem raspi)

Ein paar Configs:
Scanne alle 30 Sekunden
mosquitto_pub -t home/MyOpenMQTTGateway_ESP32_BLE/commands/MQTTtoBT/config -m '{"interval":30000}'
egal, wie weit weg
mosquitto_pub -t home/MyOpenMQTTGateway_ESP32_BLE/commands/MQTTtoBT/config -m '{"minrssi":-200}'
nur meine beiden Mis scannen
mosquitto_pub -t home/MyOpenMQTTGateway_ESP32_BLE/commands/MQTTtoBT/config -m '{"onlysensors":true}'
mosquitto_pub -t home/MyOpenMQTTGateway_ESP32_BLE/commands/MQTTtoBT/config -m '{"white-list":["C4:7C:8D:6C:9C:18","C4:7C:8D:6C:94:43"]}'

# Roborock
https://github.com/marcelrv/XiaomiRobotVacuumProtocol


# WS 3500
https://community.openhab.org/t/outside-wifi-weather-station-ws3500-and-local-customized-upload-no-cloud-needed/98756


# Trouble shooting

HueEmulation: Bei Problemen ( wenn alexa keine neuen Geräte findet), den docker mit network_type=host starten

Homegear mag nicht mit CUL (device busy z.B.)
'docker restart homegear'

Addons für 2.5.x nachladen: https://v2.openhab.org/download/ und ins addons Verzeichniss kopieren
pnp Pr
# oh karaf console (default pw=habopen)
  ssh -p 8101 openhab@localhost

# nach einem Reset muss u.U. Miele neu gepaired werden
http://homie/mielecloud

# shell shortcuts
docker exec -ti influxDB influx -database openhab_db -precision rfc3339

## Todo

- Sitemap
- Harmony
- Alexa
- Passwörter und Keys für PI User, raoul


Und sonst so
- Alles aus
- Media und Co
- Uhrzeit
- Radio



## Thanks

special thanks to the [community](community.openhab.org)
and [Thom Dietrich](https://github.com/ThomDietrich/)
and [Kevin Schweiger](https://schweigerstechblog.de/roborock-s7-in-openhab-integrieren-binding-einrichten/)
