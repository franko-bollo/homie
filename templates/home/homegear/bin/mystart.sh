#!/bin/bash

sed "s:^/usr/bin/homegear-management:# /usr/bin/homegear-management:g" -i /start.sh
sed "s:^/usr/bin/homegear-webssh:# /usr/bin/homegear-webssh:g" -i /start.sh
sed "s:^/usr/bin/homegear-influxdb:# /usr/bin/homegear-influxdb:g" -i /start.sh

/start.sh