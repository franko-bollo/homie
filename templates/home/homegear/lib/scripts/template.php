#!/usr/bin/env php

{% if item.type == "HM-TC-IT-WM-W-EU" %}
  {% set prefixlist = [ "P1_", "P2_", "P3_"] %}
{% else %}
  {% set prefixlist = [ "" ] %}
{% endif %}

{% set data = {} %}

{# set default values #}
{% for prefix in prefixlist %}
    {% for day in [ "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday" ] %}
      {% for pos in range(1,14) %}
        {% set dummy = data.update({day + '_temperature_' + pos|string : 19.5}) %}
        {% set dummy = data.update({day + '_endtime_' + pos|string : 1440}) %}
      {% endfor %}
    {% endfor %}
{% endfor %}

{# set user values #}

{% for prefix in prefixlist %}
    {% for entry in item.weekplan %}
      {% for day in entry.days %}
        {% for endtime in entry.endtime %}
          {% set time= endtime.split(':') %}
          {% set dummy = data.update({day + '_endtime_' + loop.index|string : (time.0 | int ) * 60  + (time.1 | int )}) %}
        {% endfor %}   
        {% for temperature in entry.temperature %}
          {% set dummy = data.update({day + '_temperature_' + loop.index|string : temperature }) %}
        {% endfor %}        
      {% endfor %}
    {% endfor %}
{% endfor %}

{# print #}

<?php
{# todo get rid of homegear id  print_v($hg->listDevices()); #}
$device={{ item.homegear_id }};


$array = array();
{% for prefix in prefixlist %}
    {% for day in [ "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday" ] %}
      {% for pos in range(1,14) %}
$array["{{ prefix }}TEMPERATURE_{{ day | upper }}_{{ pos }}"] = {{ data[ day + "_temperature_" + pos | string ] }};
$array["{{ prefix }}ENDTIME_{{ day | upper }}_{{ pos }}"] = {{ data[ day + "_endtime_" + pos | string ] }};
      {% endfor %}
    {% endfor %}
{% endfor %}

$array["TEMPERATURE_OFFSET"] = round(({{ item.offsetTemperature | default(0.0) }} + 3.5) * 2);

$array["SHOW_SET_TEMPERATUR"] = 0;
$array["CYCLIC_INFO_MSG"] = 1;
$array["SHOW_HUMIDITY"] = 1;

$hg = new \Homegear\Homegear();
$hg->putParamset($device, 0, "MASTER", $array);

print("set weekplan for {{ item.id }} done\n");

