
/*

test
{{ hardware.homematic  | selectattr('type', 'equalto', 'HM-TC-IT-WM-W-EU') | list | select(attribute='id') | list}}

*/

Group:Number:AVG          Heating_TargetTemps    "Raumtemperaturen Soll [%.1f °C]"              <temperature>     (Heating_Group)
Group:Switch:OR(ON, OFF)  Heating_ValveSettings  "Aktive Heizkörper [%d]"                       <heating>         (Heating_Group)
Group                     Heating_OpModes        "Raumtemperaturen Mode [%.1f °C]"              <"heating-off">   (Heating_Group)
Group:Number:AVG          Heating_TargetPresets  "Raumtemperaturen Soll Zeitprogramm [%.1f °C]" <line>            (Heating_Group)
Group:Number:SUM          Heating_WindowModes    "Fensterabsenkung"                             <window>          (Heating_Group)
Group:Contact:OR(OPEN, CLOSED)  Window_Contact_State  "Fenster Kontakte [%d]"                       <window>
Group:Contact:OR(OPEN, CLOSED)  Door_Contact_State  "Tür Kontakte [%d]"                       <door>
Group:Switch:OR(ON, OFF)  SmokeDetector_State  "Rauchmelder [%d]"                       <fire>
Group:Switch:OR(ON, OFF)   Lights     "Homematic Lampen"        <none>      (Whg)

                                                      
{% for device in hardware.homematic %}

{% if device.type == "HM-CC-RT-DN" %}
// ### Heizungsthermostat ################################################

Number {{ device.location }}_Heizung_RSSI              "{{ device.location }}_Heizung RSSI [%d dBm]"                                  <"signal-3">      ({{ device.location }}_Heating_info,RSSI_Group)               {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#RSSI_DEVICE"}
Switch {{ device.location }}_Heizung_Unreach           "{{ device.location }}_Heizung unreachable"                                    <siren>           ({{ device.location }}_Heating_info,Unreach_Group)            {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#UNREACH"}
Switch {{ device.location }}_Heizung_Pending           "{{ device.location }}_Heizung config pending"                                 <siren>           ({{ device.location }}_Heating_info,Pending_Group)            {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#CONFIG_PENDING"}
Number {{ device.location }}_Heizung_Battery           "{{ device.location }}_Heizung Batteriezustand [%.1f V]"                       <"battery-100">   ({{ device.location }}_Heating_info,Bat_Volts)                {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:4#BATTERY_STATE"}
String {{ device.location }}_Heizung_Fault             "{{ device.location }}_Heizung Fehlerwert [MAP({{ device.type }}.map):%s]"           <error2>          ({{ device.location }}_Heating_info,Error_Group)              {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:4#FAULT_REPORTING"}
Number {{ device.location }}_Heizung_ActTemp           "{{ device.location }}_Heizung Ist-Temperatur [%.1f °C]"                       <temperature>     ({{ device.location }}_Heating,{{ device.location }},Temperatures)          {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:4#ACTUAL_TEMPERATURE"}
Number {{ device.location }}_Heizung_SetTemp           "{{ device.location }}_Heizung Soll-Temperatur [%.1f °C]"                      <temperature>     ({{ device.location }}_Heating,{{ device.location }},Heating_TargetTemps)   {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:4#SET_TEMPERATURE"}
Number {{ device.location }}_Heizung_Valve             "{{ device.location }}_Heizung Ventilstellung [%d %%]"                          <heating>         ({{ device.location }}_Heating,{{ device.location }},Heating_ValveSettings) {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:4#VALVE_STATE"}
String {{ device.location }}_Heizung_Mode              "{{ device.location }}_Heizung Betriebsart [MAP({{ device.type }}.map):%s]"          <"heating-off">   ({{ device.location }}_Heating_info,{{ device.location }},Heating_OpModes)       {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:4#CONTROL_MODE"}
Switch {{ device.location }}_Heizung_Auto              "{{ device.location }}_Heizung Auto-Mode"                                      <temperature>     ({{ device.location }}_Heating_info)                          {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:4#AUTO_MODE"}
Number {{ device.location }}_Heizung_Manu              "{{ device.location }}_Heizung Manu-Mode [%.1f °C]"                            <"heating-40">    ({{ device.location }}_Heating_info)                          {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:4#MANU_MODE"}
Switch {{ device.location }}_Heizung_BoostMode         "{{ device.location }}_Heizung Boost-Mode"                                     <fire>            ({{ device.location }}_Heating_info)                          {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:4#BOOST_MODE"}
Number {{ device.location }}_Heizung_BoostTime         "{{ device.location }}_Heizung Boost-Restdauer [%d min]"                       <"clock-on">      ({{ device.location }}_Heating_info)                          {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:4#BOOST_STATE"}
Switch {{ device.location }}_Heizung_WindowMode        "{{ device.location }}_Heizung Fensterabsenkung"                               <window>          ({{ device.location }}_Heating_info,{{ device.location }},Heating_WindowModes)     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:3#WINDOW_STATE"}
// Number {{ device.location }}_Heizung_Preset            "{{ device.location }}_Heizung Soll-Temperatur Schnellauswahl"                 <"heating-60">    ({{ device.location }}_Heating_info)
Number {{ device.location }}_Heizung_PresetTemp        "{{ device.location }}_Heizung Soll-Temperatur Zeitprogramm [%.1f °C]"         <line>            ({{ device.location }}_Heating_info,Heating_TargetPresets)
String {{ device.location }}_Heizung_Summary           "{{ device.location }}_Heizung Übersicht [%s]"                                 <thermometer_red> ({{ device.location }}_Heating_info,{{ device.location }})

{% elif device.type == "HM-TC-IT-WM-W-EU" %}

// ### Wandthermostat ################################################

Number {{ device.location }}_Wandthermostat_RSSI              "{{ device.location }}_Wandthermostat RSSI [%d dBm]"                                  <"signal-3">      ({{ device.location }}_Heating_info,RSSI_Group)               {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#RSSI_DEVICE"}
Switch {{ device.location }}_Wandthermostat_Unreach           "{{ device.location }}_Wandthermostat unreachable"                                    <siren>           ({{ device.location }}_Heating_info,Unreach_Group)            {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#UNREACH"}
Switch {{ device.location }}_Wandthermostat_Pending           "{{ device.location }}_Wandthermostat config pending"                                 <siren>           ({{ device.location }}_Heating_info,Pending_Group)            {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#CONFIG_PENDING"}
Number {{ device.location }}_Wandthermostat_Battery           "{{ device.location }}_Wandthermostat Batteriezustand [%.1f V]"                       <"battery-100">   ({{ device.location }}_Heating_info,Bat_Volts)                {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#BATTERY_STATE"}
Number {{ device.location }}_Wandthermostat_LoweringMode      "{{ device.location }}_Wandthermostat Lowering Mode [%.1f V]"                       <"window">   ({{ device.location }}_Heating_info)                {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#LOWERING_MODE"}
Number {{ device.location }}_Wandthermostat_ActTemp           "{{ device.location }}_Wandthermostat Ist-Temperatur [%.1f °C]"                       <temperature>     ({{ device.location }}_Heating,{{ device.location }},Temperatures)          {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#ACTUAL_TEMPERATURE"}
Number {{ device.location }}_Wandthermostat_ActHumidity       "{{ device.location }}_Wandthermostat Luftfeuchtigkeit [%.1f]"                       <humidity>     (Humidity,{{ device.location }})          {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#ACTUAL_HUMIDITY"}
Number {{ device.location }}_Wandthermostat_SetTemp           "{{ device.location }}_Wandthermostat Soll-Temperatur [%.1f °C]"                      <temperature>     ({{ device.location }}_Heating,{{ device.location }},Heating_TargetTemps)   {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#SET_TEMPERATURE"}
String {{ device.location }}_Wandthermostat_Mode              "{{ device.location }}_Wandthermostat Betriebsart [MAP({{ device.type }}.map):%s]"          <"heating-off">   ({{ device.location }}_Heating_info,{{ device.location }},Heating_OpModes)       {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#CONTROL_MODE"}
Switch {{ device.location }}_Wandthermostat_Auto              "{{ device.location }}_Wandthermostat Auto-Mode"                                      <temperature>     ({{ device.location }}_Heating_info)                          {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#AUTO_MODE"}
Number {{ device.location }}_Wandthermostat_Manu              "{{ device.location }}_Wandthermostat Manu-Mode [%.1f °C]"                            <"heating-40">    ({{ device.location }}_Heating_info)                          {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#MANU_MODE"}
Switch {{ device.location }}_Wandthermostat_BoostMode         "{{ device.location }}_Wandthermostat Boost-Mode"                                     <fire>            ({{ device.location }}_Heating_info)                          {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#BOOST_MODE"}
Number {{ device.location }}_Wandthermostat_BoostTime         "{{ device.location }}_Wandthermostat Boost-Restdauer [%d min]"                       <"clock-on">      ({{ device.location }}_Heating_info)                          {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#BOOST_STATE"}
Switch {{ device.location }}_Wandthermostat_WindowMode        "{{ device.location }}_Wandthermostat Fensterabsenkung"                               <window>          ({{ device.location }}_Heating_info,{{ device.location }},Heating_WindowModes)     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:3#WINDOW_STATE"}
Number {{ device.location }}_Wandthermostat_Preset            "{{ device.location }}_Wandthermostat Soll-Temperatur Schnellauswahl"                 <"heating-60">    ({{ device.location }}_Heating_info)
Number {{ device.location }}_Wandthermostat_PresetTemp        "{{ device.location }}_Wandthermostat Soll-Temperatur Zeitprogramm [%.1f °C]"         <line>            ({{ device.location }}_Heating_info,Heating_TargetPresets)
String {{ device.location }}_Wandthermostat_Summary           "{{ device.location }}_Wandthermostat Übersicht [%s]"                                 <thermometer_red> ({{ device.location }}_Heating_info,{{ device.location }})



{% elif device.type == "HM-Sec-SC-2" or device.type == "HM-Sec-SCo" %}

// ### Tür Fenster Kontakt ################################################
{% if device.location == "FV" %}
Contact {{ device.location }}_Contact_State            "{{ rooms[ device.location ].name }} Tür [MAP(window.map):%s]"                      <contact>   ({{ device.location }}, {{ device.location }}_Door, Door_Contact_State)     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:1#STATE"}
{% else %}
Contact {{ device.location }}_Contact_State            "{{ rooms[ device.location ].name }} Fenster [MAP(window.map):%s]"                      <contact>   ({{ device.location }}, {{ device.location }}_Window, Window_Contact_State)     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:1#STATE"}
{% endif %}

String  {{ device.location }}_Contact_Error            "{{ rooms[ device.location ].name }} Kontakt Fehler [MAP({{ device.type }}.map):%s]"           <error2>    (Error_Group) {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:1#ERROR"}
Number  {{ device.location }}_Contact_RSSI             "{{ rooms[ device.location ].name }} Kontakt RSSI [%d dBm]"                                         (RSSI_Group)     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#RSSI_DEVICE"}
Switch  {{ device.location }}_Contact_Unreach          "{{ rooms[ device.location ].name }} Kontakt unreachable"                               <siren>     (Unreach_Group)  {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#UNREACH"}
Switch  {{ device.location }}_Contact_Pending          "{{ rooms[ device.location ].name }} Kontakt config pending"                            <siren>     (Pending_Group)  {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#CONFIG_PENDING"}
Switch  {{ device.location }}_Contact_LowBat           "{{ rooms[ device.location ].name }} Kontakt Batterie"                                  <battery>   (Bat_Binary)     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:1#LOWBAT"}

{% elif device.type == "HM-Sec-SD" %}

// ### Rauchmelder ################################################
Contact {{ device.location }}_SmokeDetector_State            "{{ rooms[ device.location ].name }} Rauchmelder %s"                      <contact>   ({{ device.location }}, SmokeDetector_State)     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:1#STATE"}

String  {{ device.location }}_SmokeDetector_Error            "{{ rooms[ device.location ].name }} Rauchmelder Fehler [MAP({{ device.type }}.map):%s]"           <error2>    (Error_Group) {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:1#ERROR"}
Number  {{ device.location }}_SmokeDetector_RSSI             "{{ rooms[ device.location ].name }} Rauchmelder RSSI [%d dBm]"                                         (RSSI_Group)     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#RSSI_DEVICE"}
Switch  {{ device.location }}_SmokeDetector_Unreach          "{{ rooms[ device.location ].name }} Rauchmelder unreachable"                               <siren>     (Unreach_Group)  {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#UNREACH"}
Switch  {{ device.location }}_SmokeDetector_Pending          "{{ rooms[ device.location ].name }} Rauchmelder config pending"                            <siren>     (Pending_Group)  {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#CONFIG_PENDING"}
Switch  {{ device.location }}_SmokeDetector_LowBat           "{{ rooms[ device.location ].name }} Rauchmelder Batterie"                                  <battery>   (Bat_Binary)     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#LOWBAT"}

{% elif device.type == "HM-PB-2-WM55" %}

// ### Wandtaster ################################################

String  {{ device.location }}_Switch_Error            "{{ rooms[ device.location ].name }} Schalter Fehler [MAP({{ device.type }}.map):%s]"           <error2>    (Error_Group) {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:1#ERROR"}
Number  {{ device.location }}_Switch_RSSI             "{{ rooms[ device.location ].name }} Schalter RSSI [%d dBm]"                                         (RSSI_Group)     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#RSSI_DEVICE"}
Switch  {{ device.location }}_Switch_Unreach          "{{ rooms[ device.location ].name }} Schalter unreachable"                               <siren>     (Unreach_Group)  {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#UNREACH"}
Switch  {{ device.location }}_Switch_Pending          "{{ rooms[ device.location ].name }} Schalter config pending"                            <siren>     (Pending_Group)  {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#CONFIG_PENDING"}
Switch  {{ device.location }}_Switch_LowBat           "{{ rooms[ device.location ].name }} Schalter Batterie"                                  <battery>   (Bat_Binary)     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#LOWBAT"}


{% elif device.type == "HM-ES-PMSw1-Pl" %}

// ### Steckdose  ################################################

Switch {{ device.location }}_{{ device.name }}_State        "{{ device.name }} Zustand"                     ({{ device.location }})                       {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:1#STATE"}
Number {{ device.location }}_{{ device.name }}_EnergyCount  "{{ device.name }} Energiezähler [%.0f Wh]"     ({{ device.location }},EnergyCons)            {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#ENERGY_COUNTER"}
Number {{ device.location }}_{{ device.name }}_Power        "{{ device.name }} Leistung [%.0f W]"           <energy> ({{ device.location }},Wattages)     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#POWER"}
Number {{ device.location }}_{{ device.name }}_Current      "{{ device.name }} Strom [%.1f mA]"             ({{ device.location }})                       {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#CURRENT"}
Number {{ device.location }}_{{ device.name }}_Voltage      "{{ device.name }} Spannung [%.0f V]"           ({{ device.location }})                       {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#VOLTAGE"}
Number {{ device.location }}_{{ device.name }}_Frequency    "{{ device.name }} Frequenz [%.1f Hz]"          ({{ device.location }})                       {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:2#FREQUENCY"}
Number {{ device.location }}_{{ device.name }}_RSSI         "{{ device.name }} RSSI [%d dBm]"               (RSSI_Group)               {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#RSSI_DEVICE"}
Switch {{ device.location }}_{{ device.name }}_Unreach      "{{ device.name }} unreachable"                 <siren>     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#UNREACH"}
Switch {{ device.location }}_{{ device.name }}_Pending      "{{ device.name }} config pending"              <siren> (Pending_Group)    {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#CONFIG_PENDING"}


{% elif device.type == "HM-LC-Dim1T-CV" or device.type == "HM-LC-Dim1T-FM-LF" %}

// ### Dimmer ################################################

Dimmer {{ device.location }}_{{ device.name }}_Level       "{{ device.name }} [%d %%]"                                 (HM_{{ device.name }}, {{ device.location }}_Lights, Lights)      {autoupdate="false", channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:1#LEVEL"}
Number {{ device.location }}_{{ device.name }}_RSSI        "{{ device.name }} RSSI [%d dBm]"                           (RSSI_Group)     {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#RSSI_DEVICE"}
Switch {{ device.location }}_{{ device.name }}_Unreach     "{{ device.name }} unreachable"                 <siren>     (Unreach_Group)  {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#UNREACH"}
Switch {{ device.location }}_{{ device.name }}_Pending     "{{ device.name }} config pending"              <siren>     (Pending_Group)  {channel="homematic:HG-{{ device.type }}:ccu:{{ device.id }}:0#CONFIG_PENDING"}

{% endif %}
{% endfor %}