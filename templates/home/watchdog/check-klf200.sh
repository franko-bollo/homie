#!/bin/sh

CMD="nc -zv velux-klf-200.fritz.box 51200"

echo $(date) checke klf >> /var/log/watchdog.log

if ! $CMD ; then
  # check twice
  sleep 90

  if ! $CMD ; then
    sleep 90

    if ! $CMD ; then
      echo $(date) klf ist nicht erreichbar, restart >> /var/log/watchdog.log
      /usr/local/bin/uhubctl -l 1-1 -a cycle -d 5
    fi
  fi

fi
