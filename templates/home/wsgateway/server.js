const express = require('express');
const app = express();
const port = 9000;

const mqtt = require('mqtt');
const client  = mqtt.connect('mqtt://homie.fritz.box');
const cachedData = {};

function f2c(f) {
  // fahrenheit to celcius, returns a string
  f = parseFloat(f);
  if (isNaN(f)) {
    return null;
  }
  let c = (f - 32.0) * 5.0 / 9.0;
  return `${Math.round(c * 100) / 100.0}`;
}

function mph2kmh(mph) {
  mph = parseFloat(mph);
  if (isNaN(mph)) {
    return null;
  }
  let kmh = mph * 1.609;
  return `${Math.round(kmh * 100) / 100.0}`;
}

function inch2cm(inch) {
  inch = parseFloat(inch);
  if (isNaN(inch)) {
    return null;
  }
  let cm = inch / 2.54;
  return `${Math.round(cm * 100) / 100.0}`;
}

function inhg2hpa(inhg) {
  //  Inch of mercury to Hektopascal
  inhg = parseFloat(inhg);
  if (isNaN(inhg)) {
    return null;
  }
  let hpa = inhg * 33.86389;
  return `${Math.round(hpa * 100) / 100.0}`;
}


function send2mqtt(req, propName, topic = propName, convert) {
  let val = req.query[propName];
  if (convert) {
    val = convert(val);
  }
  if (val === '' || val === null) {
    return;
  }

  if (cachedData[topic] === val) {
    return;
  }
  cachedData[topic] = val;
  if ( client.connected===true ) {
    console.log(`sending ${topic} = ${val};`);
    client.publish(`/ws3500/${topic}`, val);
  }


}



app.get('/', (req, res) => {

  send2mqtt(req, 'tempf', 'temperature', f2c);
  send2mqtt(req, 'dewptf', 'dewpoint', f2c);
  send2mqtt(req, 'windchillf', 'windchill', f2c);
  send2mqtt(req, 'humidity');
  send2mqtt(req, 'winddir', 'winddir');
  send2mqtt(req, 'windspeedmph', 'windspeed', mph2kmh);
  send2mqtt(req, 'windgustmph', 'windgust', mph2kmh);
  send2mqtt(req, 'absbaromin', 'abspressure', inhg2hpa);
  send2mqtt(req, 'baromin', 'pressure', inhg2hpa);

  send2mqtt(req, 'rainin', 'rain', inch2cm);
  send2mqtt(req, 'dailyrainin', 'dailyrain', inch2cm);
  send2mqtt(req, 'weeklyrainin', 'weeklyrain', inch2cm);
  send2mqtt(req, 'monthlyrainin', 'monthlyrain', inch2cm);
  send2mqtt(req, 'solarradiation');
  send2mqtt(req, 'UV');

  res.send('')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})


/* Sample data
  ID: '2',
  PASSWORD: '3',
  indoortempf: '73.0',
  tempf: '38.3',
  dewptf: '35.2',
  windchillf: '38.3',
  indoorhumidity: '50',
  humidity: '89',
  windspeedmph: '2.9',
  windgustmph: '4.5',
  winddir: '324',
  absbaromin: '30.068',
  baromin: '29.749',
  rainin: '0.047',
  dailyrainin: '0.240',
  weeklyrainin: '0.319',
  monthlyrainin: '0.319',
  solarradiation: '0.00',
  UV: '0',
  dateutc: '2022-02-10 21:36:26',
  softwaretype: 'EasyWeatherV1.6.1',
  action: 'updateraw',
  realtime: '1',
  rtfreq: '5'
}

*/